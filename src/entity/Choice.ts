import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne } from "typeorm";
import { type } from "os";
import { Article } from "./Article";

@Entity()
export class Choice {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    answer: String;

    @Column({ select: false })
    status: String;

    @CreateDateColumn()
    createdAt: Date;

    @ManyToOne(type => Article, article => article.choices)
    article: Article;

}
