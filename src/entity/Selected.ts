import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne } from "typeorm";
import { Player } from "./Player";

@Entity()
export class Selected {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    article_id: number;

    @CreateDateColumn()
    createdAt: Date;

    @ManyToOne(type => Player, play => play.selectes)
    play: Player;
}
