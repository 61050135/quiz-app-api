import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, OneToMany } from "typeorm";
import { Quize } from "./Quize";
import { type } from "os";
import { Choice } from "./Choice";

@Entity()
export class Article {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    question: String;

    @CreateDateColumn()
    createdAt: Date;

    @ManyToOne(type => Quize, quize => quize.articles)
    quize: Quize;

    @OneToMany(type => Choice, choices => choices.article)
    choices: Choice[];

}
