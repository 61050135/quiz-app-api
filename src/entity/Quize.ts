import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, OneToMany } from "typeorm";
import { Article } from "./Article";
import { Player } from "./Player";

@Entity()
export class Quize {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    quize_name: String;

    @Column("longtext")
    creator: String;

    @Column() //false private true private
    status: number;

    @Column({ select: false })
    code_for_delete: String;

    @Column({ default: null })
    deletedAt: Date;

    @CreateDateColumn()
    createdAt: Date;

    @OneToMany(type => Article, articles => articles.quize)
    articles: Article[];

    @OneToMany(type => Player, players => players.quize)
    players: Player[];

}
