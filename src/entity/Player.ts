import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, OneToMany } from "typeorm";
import { Quize } from "./Quize";
import { Selected } from "./Selected"

@Entity()
export class Player {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: String;

    @Column()
    point: number;

    @CreateDateColumn()
    createdAt: Date;

    @ManyToOne(type => Quize, quize => quize.players)
    quize: Quize;

    @OneToMany(type => Selected,selectes => selectes.play)
    selectes:Selected[];
}
