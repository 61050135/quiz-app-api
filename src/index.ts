import "reflect-metadata";
import { createConnection } from "typeorm";
const bodyParser = require('body-parser')
const express = require('express'),
    app = express(),
    config = require('./configs/app'),
    cors = require('cors');


createConnection();

app.use(cors())


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

// Routes
app.use(require('./routes'))



// Start Server
const server = app.listen(config.port, () => {
    let host = config.host
    let port = config.port
    console.log(`Server is running at http://${host}:${port}`)
})

