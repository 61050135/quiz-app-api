import { getRepository } from "typeorm";
import { Quize } from "../entity/Quize";
import { Article } from "../entity/Article";
import { Choice } from "../entity/Choice";
import { Player } from "../entity/Player";
const axios = require('axios');

const methods = {
    async onTest(req, res) {
        let c = await axios.get("http://quiz-app-api.eastus.azurecontainer.io:3000/api/v1/quize?page=1&items=10");
        console.log(c)
    },
    async getQuizes(req, res) {
        try {
            let { page, items } = req.query;
            if (!page) page = 1;
            if (!items) items = 10;
            page = parseInt(page);
            items = parseInt(items);
            let quize_list = await getRepository(Quize).createQueryBuilder('q')
                .where("q.status =:status", { status: 1 })
                .andWhere("q.deletedAt IS NULL")
                .orderBy("q.id", "DESC")
                .skip((page - 1) * items)
                .take(items)
                .getMany();
            let totalItem = await getRepository(Quize).createQueryBuilder('q')
                .where("q.status =:status", { status: 1 })
                .andWhere("q.deletedAt IS NULL")
                .getCount();
            return res.status(200).send({
                message: "Retrive data success full",
                page,
                perPage: items,
                totalItem,
                quize_list
            })

        } catch (error) {
            console.log(error);
            res.status(error.status).send(error.message)
        }
    },

    async OnCreate(req, res) {
        try {
            let { creator, quize_name, status, code_for_delete } = req.body;
            let quize = new Quize();
            quize.creator = creator;
            quize.quize_name = quize_name;
            quize.status = status;
            quize.code_for_delete = code_for_delete;
            let result = await getRepository(Quize).save(quize);
            return res.status(201).send({
                message: "Create quize success full",
                result: {
                    id: result.id,
                    quize_name: result.quize_name,
                    creator: result.creator,
                    status: result.status
                }
            })
        } catch (error) {
            console.log(error);
            res.status(error.status).send(error.message)
        }
    },

    async OnDelete(req, res) {
        try {
            let { code_for_delete, id } = req.params;
            console.log(code_for_delete + " " + id)
            let check = await getRepository(Quize).createQueryBuilder('q')
                .where("q.code_for_delete =:code_for_delete", { code_for_delete })
                .andWhere("q.id =:id", { id })
                .andWhere("q.deletedAt IS NULL")
                .getOne();
            if (!check) {
                return res.status(400).send({
                    message: "You don't have permision"
                });
            }
            check.deletedAt = new Date();
            await getRepository(Quize).save(check);
            return res.status(200).send({
                message: "Delete item success full"
            });
        } catch (error) {
            console.log(error);
            res.status(error.status).send(error.message)
        }
    },

    async OnCreateArticleandChoice(req, res) {
        try {
            let { article } = req.body;
            let { id } = req.params;
            console.log(article.length)
            let quize = await getRepository(Quize).createQueryBuilder("q").where("q.id =:id", { id }).getOne();
            for (let i = 0; i < article.length; i++) {
                console.log(article[i].choice)
                let article_c = new Article();
                article_c.question = article[i].question;
                article_c.quize = quize;
                let ac = await getRepository(Article).save(article_c);
                for (let j = 0; j < article[i].choice.length; j++) {
                    let choice = new Choice();
                    choice.answer = article[i].choice[j].answer;
                    choice.status = article[i].choice[j].status;
                    choice.article = ac;
                    await getRepository(Choice).save(choice);
                }
            }
            return res.status(200).send({
                message: "Create article sucess"
            });
        } catch (error) {
            console.log(error);
            res.status(error.status).send(error.message)
        }
    },

    async getQuizeArticle(req, res) {
        try {
            let { id } = req.params;
            let quize = await getRepository(Quize).createQueryBuilder("q")
                .andWhere("q.id =:id", { id })
                .leftJoinAndSelect("q.articles", "a")
                .leftJoinAndSelect("a.choices", "c")
                .getMany();
            return res.status(200).send({
                message: "Retrive article sucess",
                quize
            });
        } catch (error) {
            console.log(error);
            res.status(error.status).send(error.message)
        }
    },

    async getBoard(req, res) {
        try {
            let { id } = req.params;
            let { page, perPage } = req.query;
            let leaderboard = await getRepository(Player).createQueryBuilder("p")
                .leftJoin("p.quize", "q")
                .where("q.id =:id", { id })
                .orderBy("p.point", "DESC")
                .skip((page - 1) * perPage)
                .take(perPage)
                .getMany();
            let totalItem = await getRepository(Player).createQueryBuilder("p")
                .leftJoin("p.quize", "q")
                .where("q.id =:id", { id }).getCount();
            return res.status(200).send({
                message: "Retrive leaderboard sucess",
                page,
                perPage,
                totalItem,
                leaderboard,
            });
        } catch (error) {
            console.log(error);
            res.status(error.status).send(error.message)
        }
    },


}



module.exports = { ...methods }