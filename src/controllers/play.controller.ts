import { getRepository } from "typeorm";
import { Article } from "../entity/Article";
import { Player } from "../entity/Player";
import { Quize } from "../entity/Quize";
import { Selected } from "../entity/Selected";


const methods = {
    async sendAnsSelect(req, res) {
        try {
            let { id } = req.params;
            let { player_id, article_id, answer_id } = req.body;
            let checked = await getRepository(Player).createQueryBuilder("p")
                .where("p.id =:player_id", { player_id })
                .leftJoin("p.quize", "q")
                .andWhere("q.id =:id", { id })
                .getOne();

            if (!checked) {
                return res.status(200).send({
                    message: "Don't have quize or player"
                });
            }
            let check = await getRepository(Player).createQueryBuilder("p")
                .where("p.id =:player_id", { player_id })
                .leftJoinAndSelect("p.selectes", "s")
                .andWhere("s.article_id =:article_id", { article_id })
                .getOne();
            if (check) {
                return res.status(200).send({
                    message: "You have been do this"
                });
            }
            let count = await getRepository(Article).createQueryBuilder("a")
                .where("a.id =:id", { id: article_id })
                .leftJoinAndSelect("a.choices", "c")
                .andWhere("c.id =:ids", { ids: answer_id })
                .andWhere("c.status =:num", { num: "1" })
                .getCount();
            console.log(count)
            let selectes = new Selected();
            selectes.article_id = article_id;
            selectes.play = checked;
            await getRepository(Selected).save(selectes);
            checked.point = (checked.point + count);
            let result = await getRepository(Player).save(checked);
            return res.status(200).send({
                message: "Success",
                result
            });
        } catch (error) {
            console.log(error);
            res.status(error.status).send(error.message)
        }
    },
    async createPlayer(req, res) {
        try {
            let { player, quiz_id } = req.body;
            let checked_quize = await getRepository(Quize).createQueryBuilder("q")
                .where("q.id =:quiz_id", { quiz_id })
                .getOne();
            console.log(checked_quize)
            if (!checked_quize) {
                return res.status(200).send({
                    message: "Don't have quize"
                });
            }
            let check_username = await getRepository(Player).createQueryBuilder("p")
                .where("p.name =:player", { player })
                .innerJoin("p.quize", "q")
                .andWhere("q.id =:quiz_id", { quiz_id })
                .getOne();
            console.log(player)
            console.log(check_username)

            if (check_username) {
                return res.status(200).send({
                    message: "Already have this player name",
                    check_username
                });
            }
            let play = new Player();
            play.name = player;
            play.quize = checked_quize;
            play.point = 0;
            let result = await getRepository(Player).save(play);
            return res.status(200).send({
                message: "Success",
                result
            });
        } catch (error) {
            console.log(error);
            res.status(error.status).send(error.message)
        }
    }
}



module.exports = { ...methods }