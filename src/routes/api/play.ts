const router_play = require('express').Router()
const controller_play = require('../../controllers/play.controller')


router_play.post('/create',  controller_play.createPlayer)


router_play.post('/:id',  controller_play.sendAnsSelect)




module.exports = router_play;
