const router_quize = require('express').Router()
const controller_quize = require('../../controllers/quize.controller')

router_quize.get('/',  controller_quize.getQuizes)

router_quize.get('/:id',  controller_quize.getQuizeArticle)

router_quize.post('/',  controller_quize.OnCreate)

router_quize.post('/article-choice/:id',  controller_quize.OnCreateArticleandChoice)

router_quize.delete('/:id/:code_for_delete',  controller_quize.OnDelete)

router_quize.get('/leaderboard/:id',  controller_quize.getBoard)


router_quize.get('/test',  controller_quize.onTest)

module.exports = router_quize;
