FROM node:14.1-alpine AS BUILD_IMAGE

RUN apk update && apk add python make g++ && rm -rf /var/cache/apk/*

RUN mkdir -p /usr/app
WORKDIR /usr/app

COPY package.json /usr/app

RUN npm install

FROM node:14.1-alpine

RUN mkdir -p /usr/app
WORKDIR /usr/app

COPY --from=BUILD_IMAGE /usr/app/node_modules ./node_modules
COPY . /usr/app

EXPOSE 3000

CMD ["npm","run","production"]