module.exports = {
   "type": "mysql",
   "host": process.env.MYSQL_HOST,
   "port": process.env.MYSQL_PORT,
   "username": process.env.MYSQL_USERNAME,
   "password": process.env.MYSQL_PASSWORD,
   "database": process.env.DATABASE_NAME,
   "synchronize": true,
   "logging": false,
   "ssl":true,
   "entities": [
      "src/entity/**/*.ts"
   ],
   "migrations": [
      "src/migration/**/*.ts"
   ],
   "subscribers": [
      "src/subscriber/**/*.ts"
   ],
   "cli": {
      "entitiesDir": "src/entity",
      "migrationsDir": "src/migration",
      "subscribersDir": "src/subscriber"
   }
}